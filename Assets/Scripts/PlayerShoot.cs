﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

    [SerializeField]
    Shooter assaultRifle;

    void Update()
    {
        if(GameManager.Instance.InputController.Fire1)
        {
            assaultRifle.Fire();
        }
    }
}
