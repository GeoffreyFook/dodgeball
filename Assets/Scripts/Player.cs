﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveController))]
public class Player : MonoBehaviour
{
    private bool sprint;

    private Crosshair m_Crosshair;
    private InputController inputController;
    Vector2 mouseInput;

    [System.Serializable]
    public class MouseInput
    {
        public Vector2 damping;
        public Vector2 Sensitvity;
    }

    [SerializeField]MouseInput MouseControl; //this groups damping and sesitivty
    [SerializeField]float speed;

    private MoveController m_MoveController;
    public MoveController MoveController
    {
        get
        {
            if (m_MoveController == null)
                m_MoveController = GetComponent<MoveController>();

            return m_MoveController;
        }
    }

    private InputController playerInput;



    void Awake()
    {
        playerInput = GameManager.Instance.InputController;
        GameManager.Instance.LocalPlayer = this;
    }

    private Crosshair Crosshair
    {
        get
        {
            if (m_Crosshair == null)
                m_Crosshair = GetComponentInChildren<Crosshair>();
            
            return m_Crosshair;
        }
    }
    void Start ()
    {
        inputController = GameManager.Instance.InputController;
        sprint = false;
    }


    void FixedUpdate()
    {
        Vector2 direction = new Vector2(playerInput.Vertical * speed, playerInput.Horizontal * speed);
        MoveController.Move(direction);
        mouseInput.x = Mathf.Lerp(mouseInput.x, playerInput.MouseInput.x, 1f / MouseControl.damping.x);
       // mouseInput.y = Mathf.Lerp(mouseInput.y, playerInput.MouseInput.y, 1f / mouseControl.damping.y);
        transform.Rotate(Vector3.up * mouseInput.x * MouseControl.Sensitvity.x);
        //Crosshair.Lookheight(mouseInput.y * mouseControl.Sensitvity.y);

    }
}
