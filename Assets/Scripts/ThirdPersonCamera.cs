﻿using UnityEngine;
using System.Collections;
using System;

public class ThirdPersonCamera : MonoBehaviour {

    [SerializeField]
    Vector3 cameraOffset;
    [SerializeField]
    float damping;

    private Transform cameraLookTarget;
    private Player localPlayer;

    void Awake()
    {
        Debug.Log("Third person camera hit");
        GameManager.Instance.OnLocalPlayerJoined += HandleOnLocalPlayerJoined;
    }

    private void HandleOnLocalPlayerJoined(Player player)
    {
        localPlayer = player;
        cameraLookTarget = localPlayer.transform.Find("cameraLookAtTarget");

        if (cameraLookTarget == null)
            cameraLookTarget = localPlayer.transform;

    }


    private void Update()
    {
        Vector3 targetPosition = cameraLookTarget.position + localPlayer.transform.forward * cameraOffset.z
            + localPlayer.transform.up * cameraOffset.y
            + localPlayer.transform.right * cameraOffset.x;

        Quaternion targetRotation = Quaternion.LookRotation(cameraLookTarget.position - targetPosition, Vector3.up);
        transform.position = Vector3.Lerp(transform.position, targetPosition, damping * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, damping * Time.deltaTime);
    }
}
