﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

	// Use this for initialization
    [SerializeField]
    public Texture2D image;
    [SerializeField]

    int size;
    [SerializeField]

    float maxAngle;
    [SerializeField]

    float minAngle;
    [SerializeField]

    private float lookHeight;

    public void Lookheight(float value)
    {
        lookHeight += value;

        if (lookHeight > maxAngle || lookHeight < minAngle)
            lookHeight -= value;
    }

    void OnGUI()
    {
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        screenPosition.y = Screen.height - screenPosition.y;
        var rect = new Rect(screenPosition.x, screenPosition.y - lookHeight, size, size);

        GUI.DrawTexture(rect, image);
    }

    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
