﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    // Use this for initialization
    private Animator anim;
    void Start ()
    {
        anim = transform.GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        anim.SetBool("Sprint", Input.GetKey(KeyCode.LeftShift));
        anim.SetBool("Walk", Input.GetKey(KeyCode.CapsLock));
        anim.SetBool("Moving", (Input.GetKey(KeyCode.W) | Input.GetKey(KeyCode.S) | Input.GetKey(KeyCode.D) | Input.GetKey(KeyCode.A)));
        anim.SetBool("Sliding", Input.GetKeyDown(KeyCode.C));
    }
}
