﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    // Use this for initialization

    [SerializeField]
    float rateOfFire;

    [SerializeField]
    Projectile projectile;

    public Transform muzzle;

    float nextFireAllowed;
    public bool canFire;

    void Awake()
    {
      //  muzzle = transform.Find("Muzzle");
    }
    
    public virtual void Fire()
    {
        canFire = false;
        if (Time.time < nextFireAllowed)
            return;

        nextFireAllowed = Time.time + rateOfFire;

        //instantiate the projectile
        print(muzzle.position.y);
        Instantiate(projectile,new Vector3(muzzle.position.x, muzzle.position.y, muzzle.position.z) , muzzle.rotation);

        canFire = true;
    }
}
