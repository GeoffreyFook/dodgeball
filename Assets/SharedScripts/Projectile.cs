﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    public float timeToLive;
    [SerializeField]
    public float damage;
    // Use this for initialization
    void Start ()
    {
        Destroy(gameObject, timeToLive);
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
	}

    void OnTriggerEnter(Collider other)
    {
        print("hit" + other.name);
    }
}
